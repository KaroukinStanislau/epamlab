package by.training.parser;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.stream.*;

public class StaxParser extends Task {
    private String filename;
    private boolean isNamesValid = true;
    private boolean isContainsDefault = false;
    private List<String> depends;
    private List<String> names;

    private boolean checkdepends;
    private boolean checkdefault;
    private boolean checknames;
    private List<BuildFile> buildFiles = new ArrayList<>();

    public void setChecknames(boolean checknames) {
        this.checknames = checknames;
    }

    public void setCheckdefault(boolean checkdefault) {
        this.checkdefault = checkdefault;
    }

    public void setCheckdepends(boolean checkdepends) {
        this.checkdepends = checkdepends;
    }

    @Override
    public void execute() throws BuildException {
        for (BuildFile msg : buildFiles) {
            String fileXML = msg.getLocation();
            log("---------filename: " + fileXML);
            check(fileXML);

        }
    }

    public BuildFile createBuildFile() {
        BuildFile msg = new BuildFile();
        buildFiles.add(msg);
        return msg;
    }

    public class BuildFile {
        private String fileLocation;

        public BuildFile() {
        }

        public void setLocation(final String fileLocation) {
            this.fileLocation = fileLocation;
        }

        public String getLocation() {
            return fileLocation;
        }
    }


    public void check(String filename) {
        depends = new ArrayList<>();
        names = new ArrayList<>();
        String value;
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            XMLStreamReader xmlr = xmlInputFactory.createXMLStreamReader(new FileInputStream(filename));
            while (xmlr.hasNext()) {
                xmlr.next();
                if (xmlr.getEventType() == XMLStreamConstants.START_ELEMENT) {
                    for (int i = 0; i < xmlr.getAttributeCount(); i++) {
                        value = xmlr.getAttributeValue(i);
                        switch (xmlr.getAttributeLocalName(i)) {
                            case "name":
                                if (checknames) {
                                    if (!isNameTargetValid(value)) {
                                        System.out.println("isn`t valid name. Line " +
                                                xmlr.getLocation().getLineNumber() + ": " + value);
                                        isNamesValid = false;
                                    }
                                }
                                names.add(value);
                                break;
                            case "default":
                                if (checkdefault) {
                                    isContainsDefault = true;
                                }
                            case "depends":
                                if (checkdepends) {
                                    depends.add(value);
                                }
                                break;
                        }
                    }
                }
            }
            if (checkdepends) {
                log("depends valid: " + names.containsAll(depends));
            }
            if (checkdefault) {
                log("default valid: " + isContainsDefault);
            }
            if (checknames) {
                log("names valid: " + isNamesValid);
            }
        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
    }

    private boolean isNameTargetValid(String name) {
        boolean validNames = false;
        final String lettersOrDash = "([A-Za-z]|[-])*";
        Matcher n;
        n = Pattern.compile(lettersOrDash).matcher(name);
        if (n.matches())
            validNames = true;
        return validNames;
    }

}
